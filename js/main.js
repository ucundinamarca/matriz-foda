var ST = "#Stage_";
var S = "Stage_";
ivo.info({
    title: "Matriz Foda",
    autor: "Edilson Laverde Molina",
    date: "",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
var audios = [
    {
        url: "sonidos/click.mp3",
        name: "clic"
    },
    {
        url: "sonidos/TH_AEE_actREA2_013_matriz_foda_slide3.mp3",
        name: "foda1"
    },
    {
        url: "sonidos/TH_AEE_actREA2_014_matriz_foda_slide6.mp3",
        name: "foda2"
    },
    {
        url: "sonidos/check.ogg",
        name: "good"
    },
    {
        url: "sonidos/explosion.ogg",
        name: "bad"
    }
];
const HTML5 = [
    {
        id: ST + "credit_text",
        html: `
        <div class="creditos">
            <p>
                Nombre del material
                <span>Análisis y evaluación de estrategias</span>
            </p>
            <p>
                Experto en contenido
                <span>Benjamín Manjarrés Zárate</span>
            </p>
            <p>
                Diseñador Instruccional	
                <span>Laura Victoria Gómez</span>
            </p>
            <p>
                Diseño Gráfico
                <span>Andrés Guillermo Martínez Gamba</span>
            </p>
            <p>
                Programador
                <span>Edilson Laverde Molina</span>
            </p>
            
            <p>
                Diseñador Multimedia	
                <span>Luis Francisco Sierra, Ginés Velásquez</span>
            </p>
            <p>
                Imágenes e iconos	
                <span>Freepik, Flaticon, Shutterstock, Pexels</span>
            </p>
            <p>
                Oficina de Educación Virtual y a Distancia<br>
                Universidad de Cundinamarca<br>
                2022
            <p>
        </div>`
    },
    {
        id: ST + "t1",
        html: `
        <p class="text">
            Estimado estudiante, bienvenido a este apartado del curso, es momento de relacionar lo aprendido tomando en cuenta el ejercicio de matriz foda que se describe a continuación, el cual le permitirá ampliar su experiencia de aprendizaje a partir de la identificación de diferentes estrategias que le permitirán construir el mejor escenario apuesta para ejercer un liderazgo positivo en el desempeño de sus funciones.
        </p>`
    },
    {
        id: ST + "t2",
        html: `
        <p class="text">
            Teniendo en cuenta los 4 escenarios diseñados por Hassan Martínez, decidió que su escenario apuesta o conveniente para su rol de liderazgo, es el denominado “Selección Brasil”, ya que a ejemplo de Samantha, él quiere ser en un futuro un gran líder , capaz de influir positivamente en sus colaboradores.
        </p>
        <p class="text">
            No obstante, Samantha le sugiere que realice su propia matriz FODA en la que identifique las principales estrategias, que le permiten construir su escenario apuesta.
        </p>
        <p class="text">
            Por lo tanto, Hassan Martínez formula unas estrategias y le sugiere a usted ubicarlas en el cuadrante respectivo de la siguiente matriz FODA:
        </p>
        `
    },
    {
        id: ST + "t3",
        html: `
        <p class="text">
            Hassan Martínez tiene ahora la claridad de las estrategias que debe implementar para mejorar su labor de liderazgo en el área de trabajo. 
        </p>
        <p class="text">
            Le invitamos a que continúe con esta experiencia de aprendizaje, que ahora llevará a evaluar la implementación de las estrategias y analizar su viabilidad.
        </p>
        <p class="text">
            <span>¡Adelante con su proceso de aprendizaje!</span>
        </p>
        `
    }  
];
const FEEDBACK = {
    correct: `
    <div class="feedback">
        <h1>Realimentación</h1>
        <p>¡Buen trabajo¡ Ha clasificado correctamente la estrategia propuesta en el cuadrante respectivo, de acuerdo con su perfil y su naturaleza.</p>
    </div>
    `,
    incorrect: `
    <div class="feedback">
        <h1>Realimentación</h1>
        <p>¡Incorrecto! Es necesario identificar la naturaleza de la estrategia propuesta, así como, su categorización correcta en el cuadrante respectivo de la matriz FODA cruzada.</p>
    </div>
    `,
};
var dictionaryRelation = [
    {
        source: ST + "r1",
        target: ST + "game_t1",
    },
    {
        source: ST + "r2",
        target: ST + "game_t2",
    },
    {
        source: ST + "r3",
        target: ST + "game_t3",
    },
    {
        source: ST + "r6",
        target: ST + "game_t4",
    },
    {
        source: ST + "r11",
        target: ST + "game_t5",
    },
    {
        source: ST + "r13",
        target: ST + "game_t6",
    },
    {
        source: ST + "r16",
        target: ST + "game_t7",
    },
    {
        source: ST + "r17",
        target: ST + "game_t8"
    }
];
var correct = 0;
var questions = 0;
var FINISH = false;
function main(sym) {
    var t = null;
    udec = ivo.structure({
        created: function () {
            t = this;
            t.setHTML5();
            t.events();
            t.animations();
            //precarga audios//
            ivo.load_audio(audios, onComplete = function () {
                ivo(ST + "preload").hide();
                stage1.play();
            });
        },
        methods: {
            setHTML5: function () {
                let countElmentsHTML5 = HTML5.length;
                for (let i = 0; i < countElmentsHTML5; i++) {
                    let element = HTML5[i];
                    ivo(element.id).html(element.html);
                };
            },
            /*
            @type: params {string} type good or bad
            @type: params {string} message
            */  
            showNotification: function (type, message) {
                if (type === "good"){
                    ivo(ST + "alert_feedback_bad").hide();
                    ivo(ST + "alert_feedback_good").show();
                }
                if (type === "bad"){
                    ivo(ST + "alert_feedback_good").hide();
                    ivo(ST + "alert_feedback_bad").show();
                }
                ivo(ST + "alert_msg").html("<div class='alert'><h1>Realimentación</h1><p>"+message+"</p></div>");
                notification.timeScale(1).play();
            },
            events: function () {
                var t = this;
                /* ======================== Botón inicio ======================== */
                let next =  () => {
                    ivo.play("clic");
                    stage1.timeScale(4).reverse();
                    stage2.timeScale(1).play();
                }
                ivo(ST + "btn_intro_next").on("click", function () {
                    next();
                })
                .on("keypress", function (e) {
                    if (e.keyCode == 13) {
                        next();
                    }
                }).on("mouseover", function () {
                    ivo(this).css({opacity: 0.5});
                })
                .on("mouseout", function () {
                    ivo(this).css({opacity: 1});
                });
                let home = () => {
                    ivo.play("clic");
                    slider.go(1);
                    ivo(ST + "slider_title").show();
                }
                ivo(ST + "btn_home").on("click", function () {
                    home();
                })
                .on("keypress", function (e) {
                    if (e.keyCode == 13) {
                        home();
                    }
                }).on("mouseover", function () {
                    ivo(this).css({opacity: 0.5});
                })
                .on("mouseout", function () {
                    ivo(this).css({opacity: 1});
                });
                /* ========================    Slider   ======================== */
                var slider=ivo(ST+"slider").slider({
                    slides:'.sliders',
                    btn_next:ST+"btn_next",
                    btn_back:ST+"btn_back",
                    rules:function(rule){
                        console.log("rules"+rule);
                    },
                    onMove:function(page){
                        switch(page){
                            case 3:
                                ivo(ST + "slider_title").hide();
                                setTimeout(()=>{
                                    ivo(ST+"btn_back").hide();
                                    ivo(ST+"btn_next").hide();
                                },200);
                                break;

                        }

                    },
                    onFinish:function(){
                        ivo(ST+"btn_back").hide();
                        ivo(ST+"btn_next").hide();
                    }
                });
                /* ========================    Credits  ======================== */
                let openCredits = () => {
                    ivo.play("clic");
                    credits.timeScale(2).play();
                }
                ivo(ST + "btn_credit").on("click", function () {
                    openCredits();
                })
                .on("keypress", function (e) {
                    if (e.keyCode == 13) {
                        openCredits();
                    }
                }).on("mouseover", function () {
                    ivo(this).css({opacity: 0.5});
                })
                .on("mouseout", function () {
                    ivo(this).css({opacity: 1});
                });
                let closeGredits =  () => {
                    ivo.play("clic");
                    credits.timeScale(10).reverse();
                }
                ivo(ST + "credit_btn_close").on("click", function () {
                    closeGredits();
                })
                .on("keypress", function (e) {
                    if (e.keyCode == 13) {
                        closeGredits();
                    }
                }).on("mouseover", function () {
                    ivo(this).css({opacity: 0.5});
                }).on("mouseout", function () {
                    ivo(this).css({opacity: 1});
                });
                /* ========================    Bótones sonido  ======================== */
                let soundOn = (id,sound) => {
                    ivo.play("clic");
                   //verificamos si el id tiene opacidad 
                    if($(id).css("opacity")=='1'){
                        $(id).css({opacity: 0.5});
                        setTimeout(() => {
                            ivo.play(sound);
                        }, 500);
                        console.log("sonido encendido");
                    }else{  
                        $(id).css({opacity: 1});
                        ivo.stop();
                        console.log("sonido apagado");
                    }
                }
                $(ST + "btn_sound1").on("click", function () {
                    soundOn(ST + "btn_sound1", "foda1");
                })
                .on("keypress", function (e) {
                    if (e.keyCode == 13) {
                        soundOn();
                    }
                }).css({opacity: 1});
                $(ST + "btn_sound2").on("click", function () {
                    soundOn(ST + "btn_sound2", "foda2");
                })
                .on("keypress", function (e) {
                    if (e.keyCode == 13) {
                        soundOn();
                    }
                }).css({opacity: 1});
                /* ========================    Notificación  ======================== */
                let closeNotification = () => {
                    ivo.play("clic");
                    notification.timeScale(10).reverse();
                    if(FINISH){
                        //mostrar slide final//
                        slider.go(4);
                        //calculamos la nota del scorm
                        let nota = (correct / 8) * 50;
                        Scorm_mx = new MX_SCORM(false);
                        console.log("Nombre: " + Scorm_mx.info_user().name + " Id: " + Scorm_mx.info_user().id + " Nota: " + nota);
                        Scorm_mx.set_score(nota);
                    }
                }
                ivo(ST + "btn_close").on("click", function () {
                    closeNotification();
                })
                .on("keypress", function (e) {
                    if (e.keyCode == 13) {
                        closeNotification();
                    }
                }).on("mouseover", function () {
                    ivo(this).css({opacity: 0.5});
                }).on("mouseout", function () {
                    ivo(this).css({opacity: 1});
                });
                /* ========================    Dragg and Dropp  ======================== */
                
                let dragg = $(".drag").draggable({revert: true});
                let dropp = $(".drop").droppable({
                    drop: function (event, ui) {
                        let source = $(this).attr("id");
                        let target = ui.draggable.attr("id");
                        //buscamos si existe la relación
                        let relation = dictionaryRelation.find(function (element) {
                            return element.source == "#"+source && element.target == "#"+target;
                        });
                        if (relation) {
                            ui.draggable.draggable("disable");
                            $(this).droppable("disable");
                            ui.draggable.position({
                                my: "center",
                                at: "center",
                                of: $(this),
                                using: function (pos) {
                                    $(this).animate(pos, 200, "linear");
                                },
                            });
                            ui.draggable.draggable("option", "revert", false);
                            ivo.play("good");
                            t.showNotification("good","¡Buen trabajo¡ Ha clasificado correctamente la estrategia propuesta en el cuadrante respectivo, de acuerdo con su perfil y su naturaleza.");
                            correct++;
                            
                        } else {
                            ivo.play("bad");
                            t.showNotification("bad","¡Buen trabajo¡ Ha clasificado correctamente la estrategia propuesta en el cuadrante respectivo, de acuerdo con su perfil y su naturaleza.");
                            ui.draggable.position({
                                my: "center",
                                at: "center",
                                of: $(this),
                                using: function (pos) {
                                    $(this).animate(pos, 200, "linear");
                                },
                            });
                            //pintamos de rojo cuadro
                            ui.draggable.css({
                                backgroundColor: "rgba(255,0,0,0.1)"
                            });
                            ui.draggable.draggable("option", "revert", false);
                        }
                        questions ++;
                        if (questions == 8) {
                            FINISH = true;
                            slider.go(4);
                        }
                    }
                });

            },
            animations: function () {
                stage1 = new TimelineMax({
                    onComplete: function () {
                        $(ST+"btn_intro_next").addClass("animated infinite pulse");
                    }
                });
                stage1.append(TweenMax.from(ST + "stage1", .8, {y: 1300, opacity: 0}), 0);
                stage1.append(TweenMax.from(ST + "intro_title", .8, {y: -300, opacity: 0}), 0);
                stage1.append(TweenMax.from(ST + "intro_image", .8, {x: 1300, opacity: 0}), 0);
                stage1.append(TweenMax.from(ST + "logo2", .8, {x: -300, opacity: 0}), 0);
                stage1.append(TweenMax.from(ST + "btn_intro_next", .8, {x: 300, rotation:9000, opacity: 0,  ease: Elastic.easeOut.config(0.3, 0.4)}), 0);
                stage1.append(TweenMax.staggerFrom(".example", .4, {x: 100, opacity: 0, scaleY: 6, ease: Elastic.easeOut.config(0.3, 0.4)}, .2), 0);
                stage1.stop();

                stage2 = new TimelineMax({});
                stage2.append(TweenMax.from(ST + "stage2", .8, {y: 1300, opacity: 0}), 0);
                stage2.append(TweenMax.from(ST + "slider_title", .8, {y: -300, opacity: 0}), 0);
                stage2.append(TweenMax.from(ST + "slider_logo", .8, {x: 1300, opacity: 0}), 0);
                stage2.append(TweenMax.from(ST + "btn_credit", .8, {x: -300, opacity: 0}), 0);
                stage2.stop();

                notification = new TimelineMax({});
                notification.append(TweenMax.from(ST + "notification", .8, {y: -1300, opacity: 0}), 0);
                notification.append(TweenMax.from(ST + "alext_box", .8, {y: -800, opacity: 0}), 0);
                notification.append(TweenMax.from(ST + "msg", .8, {x: 400, opacity: 0}), 0);
                notification.append(TweenMax.from(ST + "btn_close", .8, {x: 300, rotation:9000, opacity: 0,  ease: Elastic.easeOut.config(0.3, 0.4)}), 0);
                notification.stop();

                credits = new TimelineMax({});
                credits.append(TweenMax.from(ST + "credits", .8, {y: 1300, opacity: 0}), 0);
                credits.append(TweenMax.from(ST + "credit_title_1", .8, {y: -300, opacity: 0}), 0);
                credits.append(TweenMax.from(ST + "credit_title_2", .8, {x: -300, opacity: 0}), 0);
                credits.append(TweenMax.from(ST + "credit_logo", .8, {x: -300, opacity: 0}), 0);
                credits.append(TweenMax.from(ST + "credit_icon", .8, {y: -300, opacity: 0}), 0);
                credits.append(TweenMax.from(ST + "credit_text", .8, {y: 300, opacity: 0}), 0);
                credits.append(TweenMax.from(ST + "credit_btn_close", .8, {x: 300, rotation:9000, opacity: 0,  ease: Elastic.easeOut.config(0.3, 0.4)}), 0);
                credits.stop();
            }
        }
    });
}