/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "both",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'stage1',
                            type: 'group',
                            rect: ['2', '0', '1025', '642', 'auto', 'auto'],
                            c: [
                            {
                                id: 'backgroud_1',
                                type: 'image',
                                rect: ['0px', '0px', '1025px', '642px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"backgroud_1.png",'0px','0px']
                            },
                            {
                                id: 'logo2',
                                type: 'image',
                                rect: ['7px', '569px', '136px', '63px', 'auto', 'auto'],
                                title: 'Logo Universidad de Cundinamarca',
                                fill: ["rgba(0,0,0,0)",im+"logo.png",'0px','0px']
                            },
                            {
                                id: 'intro_title',
                                type: 'image',
                                rect: ['45px', '21px', '846px', '118px', 'auto', 'auto'],
                                title: 'Matriz Foda. Construyendo el mejor escenario',
                                fill: ["rgba(0,0,0,0)",im+"intro_title.png",'0px','0px']
                            },
                            {
                                id: 'intro_image',
                                type: 'image',
                                rect: ['122px', '119px', '777px', '522px', 'auto', 'auto'],
                                title: 'Imagen Introducción',
                                fill: ["rgba(0,0,0,0)",im+"intro_image.png",'0px','0px']
                            },
                            {
                                id: 'btn_intro_next',
                                type: 'image',
                                rect: ['748px', '172px', '138px', '138px', 'auto', 'auto'],
                                title: 'Botón siguiente',
                                fill: ["rgba(0,0,0,0)",im+"btn_intro_next.png",'0px','0px'],
                                userClass: "pointer"
                            }]
                        },
                        {
                            id: 'stage2',
                            type: 'group',
                            rect: ['0', '0', '1027', '642', 'auto', 'auto'],
                            c: [
                            {
                                id: 'slider_backgroud',
                                type: 'image',
                                rect: ['2px', '0px', '1025px', '642px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"backgroud_1.png",'0px','0px']
                            },
                            {
                                id: 'btn_next',
                                type: 'image',
                                rect: ['523px', '535px', '46px', '46px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"btn_next.png",'0px','0px'],
                                userClass: "pointer"
                            },
                            {
                                id: 'btn_back',
                                type: 'image',
                                rect: ['461px', '535px', '46px', '46px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"btn_next.png",'0px','0px'],
                                userClass: "pointer",
                                transform: [[],['180']]
                            },
                            {
                                id: 'slider_logo',
                                type: 'image',
                                rect: ['9px', '569px', '136px', '63px', 'auto', 'auto'],
                                title: 'Logo Universidad de Cundinamarca',
                                fill: ["rgba(0,0,0,0)",im+"logo.png",'0px','0px']
                            },
                            {
                                id: 'slider_title',
                                type: 'image',
                                rect: ['0px', '14px', '620px', '94px', 'auto', 'auto'],
                                title: 'Título matriz foda construyendo el mejor escenario',
                                fill: ["rgba(0,0,0,0)",im+"title.png",'0px','0px']
                            },
                            {
                                id: 'container_slider',
                                type: 'group',
                                rect: ['85', '154', '846', '370', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'slider',
                                    type: 'group',
                                    rect: ['0px', '0px', '846', '370', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'slide1',
                                        type: 'rect',
                                        rect: ['0px', '0px', '846px', '370px', 'auto', 'auto'],
                                        fill: ["rgba(255,255,255,1)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "sliders",
                                        c: [
                                        {
                                            id: 't1',
                                            type: 'rect',
                                            rect: ['0px', '0px', '846px', '370px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,1)"],
                                            stroke: [0,"rgb(0, 0, 0)","none"],
                                            userClass: "container_text"
                                        }]
                                    },
                                    {
                                        id: 'slide2',
                                        type: 'rect',
                                        rect: ['0px', '0px', '846px', '370px', 'auto', 'auto'],
                                        fill: ["rgba(255,255,255,1)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "sliders",
                                        c: [
                                        {
                                            id: 't2',
                                            type: 'rect',
                                            rect: ['0px', '0px', '846px', '370px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,1)"],
                                            stroke: [0,"rgb(0, 0, 0)","none"],
                                            userClass: "container_text"
                                        },
                                        {
                                            id: 'btn_sound1',
                                            type: 'image',
                                            rect: ['399px', '303px', '57px', '57px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"btn_sound.png",'0px','0px'],
                                            userClass: "pointer"
                                        }]
                                    },
                                    {
                                        id: 'slide3',
                                        type: 'rect',
                                        rect: ['0px', '0px', '846px', '370px', 'auto', 'auto'],
                                        fill: ["rgba(255,255,255,1)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "sliders",
                                        c: [
                                        {
                                            id: 'game_table',
                                            type: 'image',
                                            rect: ['-52px', '-146px', '542px', '615px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"game_table.png",'0px','0px']
                                        },
                                        {
                                            id: 'btn_home',
                                            type: 'image',
                                            rect: ['856px', '396px', '57px', '73px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"btn_home.png",'0px','0px'],
                                            userClass: "pointer"
                                        },
                                        {
                                            id: 'r1',
                                            type: 'rect',
                                            rect: ['111px', '25px', '184px', '39px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,1.00)"],
                                            stroke: [0,"rgba(0,0,0,1)","none"],
                                            userClass: "drop"
                                        },
                                        {
                                            id: 'r2',
                                            type: 'rect',
                                            rect: ['111px', '70px', '184px', '49px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,1.00)"],
                                            stroke: [0,"rgba(0,0,0,1)","none"],
                                            userClass: "drop"
                                        },
                                        {
                                            id: 'r3',
                                            type: 'rect',
                                            rect: ['111px', '124px', '184px', '40px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,1.00)"],
                                            stroke: [0,"rgba(0,0,0,1)","none"],
                                            userClass: "drop"
                                        },
                                        {
                                            id: 'r4',
                                            type: 'rect',
                                            rect: ['111px', '168px', '184px', '32px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,1.00)"],
                                            stroke: [0,"rgba(0,0,0,1)","none"],
                                            userClass: "drop"
                                        },
                                        {
                                            id: 'r5',
                                            type: 'rect',
                                            rect: ['111px', '205px', '184px', '32px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,1.00)"],
                                            stroke: [0,"rgba(0,0,0,1)","none"],
                                            userClass: "drop"
                                        },
                                        {
                                            id: 'r6',
                                            type: 'rect',
                                            rect: ['111px', '263px', '184px', '54px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,1.00)"],
                                            stroke: [0,"rgba(0,0,0,1)","none"],
                                            userClass: "drop"
                                        },
                                        {
                                            id: 'r7',
                                            type: 'rect',
                                            rect: ['111px', '322px', '184px', '43px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,1.00)"],
                                            stroke: [0,"rgba(0,0,0,1)","none"],
                                            userClass: "drop"
                                        },
                                        {
                                            id: 'r8',
                                            type: 'rect',
                                            rect: ['111px', '370px', '184px', '26px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,1.00)"],
                                            stroke: [0,"rgba(0,0,0,1)","none"],
                                            userClass: "drop"
                                        },
                                        {
                                            id: 'r9',
                                            type: 'rect',
                                            rect: ['111px', '401px', '184px', '32px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,1.00)"],
                                            stroke: [0,"rgba(0,0,0,1)","none"],
                                            userClass: "drop"
                                        },
                                        {
                                            id: 'r10',
                                            type: 'rect',
                                            rect: ['111px', '435px', '184px', '32px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,1.00)"],
                                            stroke: [0,"rgba(0,0,0,1)","none"],
                                            userClass: "drop"
                                        },
                                        {
                                            id: 'r11',
                                            type: 'rect',
                                            rect: ['300px', '25px', '184px', '39px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,1.00)"],
                                            stroke: [0,"rgba(0,0,0,1)","none"],
                                            userClass: "drop"
                                        },
                                        {
                                            id: 'r12',
                                            type: 'rect',
                                            rect: ['300px', '70px', '184px', '49px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,1.00)"],
                                            stroke: [0,"rgba(0,0,0,1)","none"],
                                            userClass: "drop"
                                        },
                                        {
                                            id: 'r13',
                                            type: 'rect',
                                            rect: ['300px', '124px', '184px', '40px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,1.00)"],
                                            stroke: [0,"rgba(0,0,0,1)","none"],
                                            userClass: "drop"
                                        },
                                        {
                                            id: 'r14',
                                            type: 'rect',
                                            rect: ['300px', '168px', '184px', '32px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,1.00)"],
                                            stroke: [0,"rgba(0,0,0,1)","none"],
                                            userClass: "drop"
                                        },
                                        {
                                            id: 'r15',
                                            type: 'rect',
                                            rect: ['300px', '205px', '184px', '32px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,1.00)"],
                                            stroke: [0,"rgba(0,0,0,1)","none"],
                                            userClass: "drop"
                                        },
                                        {
                                            id: 'r16',
                                            type: 'rect',
                                            rect: ['300px', '263px', '184px', '54px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,1.00)"],
                                            stroke: [0,"rgba(0,0,0,1)","none"],
                                            userClass: "drop"
                                        },
                                        {
                                            id: 'r17',
                                            type: 'rect',
                                            rect: ['300px', '322px', '184px', '43px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,1.00)"],
                                            stroke: [0,"rgba(0,0,0,1)","none"],
                                            userClass: "drop"
                                        },
                                        {
                                            id: 'r18',
                                            type: 'rect',
                                            rect: ['300px', '370px', '184px', '26px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,1.00)"],
                                            stroke: [0,"rgba(0,0,0,1)","none"],
                                            userClass: "drop"
                                        },
                                        {
                                            id: 'r19',
                                            type: 'rect',
                                            rect: ['300px', '401px', '184px', '32px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,1.00)"],
                                            stroke: [0,"rgba(0,0,0,1)","none"],
                                            userClass: "drop"
                                        },
                                        {
                                            id: 'r20',
                                            type: 'rect',
                                            rect: ['300px', '435px', '184px', '32px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,1.00)"],
                                            stroke: [0,"rgba(0,0,0,1)","none"],
                                            userClass: "drop"
                                        },
                                        {
                                            id: 'game_t8',
                                            type: 'image',
                                            rect: ['612px', '400px', '186px', '40px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"game_t8.png",'0px','0px'],
                                            userClass: "drag"
                                        },
                                        {
                                            id: 'game_t7',
                                            type: 'image',
                                            rect: ['612px', '341px', '186px', '40px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"game_t7.png",'0px','0px'],
                                            userClass: "drag"
                                        },
                                        {
                                            id: 'game_t6',
                                            type: 'image',
                                            rect: ['612px', '283px', '186px', '40px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"game_t6.png",'0px','0px'],
                                            userClass: "drag"
                                        },
                                        {
                                            id: 'game_t5',
                                            type: 'image',
                                            rect: ['612px', '227px', '186px', '40px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"game_t5.png",'0px','0px'],
                                            userClass: "drag"
                                        },
                                        {
                                            id: 'game_t4',
                                            type: 'image',
                                            rect: ['612px', '166px', '186px', '41px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"game_t4.png",'0px','0px'],
                                            userClass: "drag"
                                        },
                                        {
                                            id: 'game_t3',
                                            type: 'image',
                                            rect: ['612px', '111px', '186px', '39px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"game_t3.png",'0px','0px'],
                                            userClass: "drag"
                                        },
                                        {
                                            id: 'game_t2',
                                            type: 'image',
                                            rect: ['612px', '46px', '186px', '41px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"game_t2.png",'0px','0px'],
                                            userClass: "drag"
                                        },
                                        {
                                            id: 'game_t1',
                                            type: 'image',
                                            rect: ['612px', '-15px', '176px', '40px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"game_t1.png",'0px','0px'],
                                            userClass: "drag"
                                        }]
                                    },
                                    {
                                        id: 'slide4',
                                        type: 'rect',
                                        rect: ['0px', '0px', '846px', '370px', 'auto', 'auto'],
                                        fill: ["rgba(255,255,255,1)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "sliders",
                                        c: [
                                        {
                                            id: 't3',
                                            type: 'rect',
                                            rect: ['0px', '0px', '846px', '370px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,1)"],
                                            stroke: [0,"rgb(0, 0, 0)","none"],
                                            userClass: "container_text"
                                        },
                                        {
                                            id: 'btn_sound2',
                                            type: 'image',
                                            rect: ['399px', '303px', '57px', '57px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"btn_sound.png",'0px','0px'],
                                            userClass: "pointer"
                                        },
                                        {
                                            id: 'escritorio-de-oficina',
                                            type: 'image',
                                            rect: ['593px', '285px', '177px', '177px', 'auto', 'auto'],
                                            opacity: '0.30894308943089',
                                            fill: ["rgba(0,0,0,0)",im+"escritorio-de-oficina.png",'0px','0px']
                                        }]
                                    }]
                                }]
                            },
                            {
                                id: 'btn_credit',
                                type: 'image',
                                rect: ['958px', '8px', '57px', '72px', 'auto', 'auto'],
                                title: 'Botón créditos',
                                fill: ["rgba(0,0,0,0)",im+"btn_credit.png",'0px','0px'],
                                userClass: "pointer"
                            }]
                        },
                        {
                            id: 'notification',
                            type: 'group',
                            rect: ['-12', '0', '1041', '666', 'auto', 'auto'],
                            c: [
                            {
                                id: 'backgroud_2',
                                type: 'image',
                                rect: ['0px', '0px', '1041px', '666px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"backgroud_2.png",'0px','0px']
                            },
                            {
                                id: 'alext_box',
                                type: 'image',
                                rect: ['60px', '114px', '922px', '411px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"alext_box.png",'0px','0px']
                            },
                            {
                                id: 'msg',
                                type: 'group',
                                rect: ['129px', '230px', '770', '177', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'alert_msg',
                                    type: 'text',
                                    rect: ['0px', '0px', '527px', '177px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​</p>",
                                    align: "left",
                                    font: ['Arial, Helvetica, sans-serif', [24, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                    textStyle: ["", "", "", "", "none"]
                                },
                                {
                                    id: 'alert_feedback',
                                    type: 'group',
                                    rect: ['556px', '0px', '214', '177', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'alert_feedback_good',
                                        type: 'image',
                                        rect: ['0px', '0px', '214px', '177px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"alert_feedback_good.png",'0px','0px']
                                    },
                                    {
                                        id: 'alert_feedback_bad',
                                        type: 'image',
                                        rect: ['0px', '0px', '214px', '177px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"alert_feedback_bad.png",'0px','0px']
                                    }]
                                }]
                            },
                            {
                                id: 'btn_close',
                                type: 'image',
                                rect: ['939px', '91px', '63px', '63px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"btn_close.png",'0px','0px'],
                                userClass: "pointer"
                            }]
                        },
                        {
                            id: 'credits',
                            type: 'group',
                            rect: ['0', '0', '1027', '642', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Rectangle',
                                type: 'rect',
                                rect: ['0px', '0px', '1025px', '640px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1.00)"],
                                stroke: [0,"rgba(0,0,0,1)","none"]
                            },
                            {
                                id: 'credit_background',
                                type: 'image',
                                rect: ['2px', '0px', '1025px', '642px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"backgroud_1.png",'0px','0px']
                            },
                            {
                                id: 'credit_logo',
                                type: 'image',
                                rect: ['9px', '569px', '136px', '63px', 'auto', 'auto'],
                                title: 'Logo Universidad de Cundinamarca',
                                fill: ["rgba(0,0,0,0)",im+"logo.png",'0px','0px']
                            },
                            {
                                id: 'credit_title_1',
                                type: 'image',
                                rect: ['0px', '14px', '620px', '94px', 'auto', 'auto'],
                                title: 'Título matriz foda construyendo el mejor escenario',
                                fill: ["rgba(0,0,0,0)",im+"title.png",'0px','0px']
                            },
                            {
                                id: 'credit_text',
                                type: 'text',
                                rect: ['386px', '127px', '396px', '496px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​</p>",
                                font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                            },
                            {
                                id: 'credit_title_2',
                                type: 'image',
                                rect: ['47px', '127px', '290px', '85px', 'auto', 'auto'],
                                title: 'título créditos',
                                fill: ["rgba(0,0,0,0)",im+"credit_title.png",'0px','0px']
                            },
                            {
                                id: 'credit_icon',
                                type: 'image',
                                rect: ['782px', '304px', '195px', '226px', 'auto', 'auto'],
                                title: 'Icono créditos',
                                fill: ["rgba(0,0,0,0)",im+"credit_icon.png",'0px','0px']
                            },
                            {
                                id: 'credit_btn_close',
                                type: 'image',
                                rect: ['945px', '12px', '63px', '63px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"btn_close.png",'0px','0px'],
                                userClass: "pointer"
                            }]
                        },
                        {
                            id: 'preload',
                            type: 'group',
                            rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-preload',
                                type: 'rect',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'logo',
                                type: 'image',
                                rect: ['462px', '270px', '100px', '100px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"logo.svg",'0px','0px']
                            }]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1024px', '640px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("index_edgeActions.js");
})("EDGE-247297");
